//
//  WeatherUITests.swift
//  WeatherUITests
//
//  Created by Sami Chaudry on 22/06/2016.
//  Copyright © 2016 Sami Chaudry. All rights reserved.
//

import XCTest

class WeatherUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLocationAccessAllowed()
    {
        XCUIDevice.sharedDevice().orientation = .FaceUp
        XCUIApplication().alerts["Allow “Weather” to access your location while you use the app?"].collectionViews.buttons["Allow"].tap()
    }
    
    func testLocationAccessDisallowed()
    {
        XCUIDevice.sharedDevice().orientation = .FaceUp
        XCUIApplication().alerts["Allow “Weather” to access your location while you use the app?"].collectionViews.buttons["Don’t Allow"].tap()
    }
    
    func testAddPlace()
    {
        let app = XCUIApplication()
        app.buttons["addButtonWhite"].tap()
        app.tables.staticTexts["Add a location"].tap()
        app.navigationBars["searchBar"].searchFields["Search"]
        app.tables.childrenMatchingType(.Cell).elementBoundByIndex(0).staticTexts["United Kingdom"].tap()
        app.navigationBars["Places"].buttons["Done"].tap()
    }
    
    func testDeletePlace()
    {
        let app = XCUIApplication()
        app.buttons["addButtonWhite"].tap()
        
        let tablesQuery = app.tables
        tablesQuery.staticTexts["London"].tap()
        tablesQuery.buttons["Delete"].tap()
        app.navigationBars["Places"].buttons["Done"].tap()
    }
    
    func testPlaceScrollView()
    {
        let elementsQuery = XCUIApplication().scrollViews.otherElements
        let staticText = elementsQuery.staticTexts["11°"]
        staticText.tap()
        staticText.swipeLeft()
    }
    
    func testWeatherScrollView()
    {
        
        let app = XCUIApplication()
        let table = app.otherElements.containingType(.Button, identifier:"addButtonWhite").childrenMatchingType(.ScrollView).element.childrenMatchingType(.ScrollView).element.childrenMatchingType(.Table).elementBoundByIndex(0)
        table.cells.containingType(.StaticText, identifier:"15:00").staticTexts["14°"].tap()
        table.cells.containingType(.StaticText, identifier:"12:00").staticTexts["13°"].swipeLeft()        
    }
}
