//
//  WeatherTests.swift
//  WeatherTests
//
//  Created by Sami Chaudry on 22/06/2016.
//  Copyright © 2016 Sami Chaudry. All rights reserved.
//

import XCTest
@testable import Weather

class WeatherTests: XCTestCase
{
    
    var viewController: ViewController!
    
    override func setUp()
    {
        super.setUp()
        
        viewController = ViewController()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    func testSetupWeatherViewsPerformance()
    {
        self.measureBlock
        {
            self.viewController.setupWeatherViews()
        }
    }
    
    func testReloadWeatherViewsPerformance()
    {
        self.measureBlock
        {
            self.viewController.places.insertObject(["place": "Black Swan", "latitude": "53.579461", "longitude": "6.075439"], atIndex: 0)
            self.viewController.reloadWeatherViews()
        }
    }
    
    func testGetForecastPerformance()
    {
        self.measureBlock
        {
            self.viewController.getForecast(["place": "Black Swan", "latitude": "53.579461", "longitude": "6.075439"])
        }
    }
 
    func testAddDefaultLocation()
    {
        self.measureBlock
            {
                self.viewController.addDefaultLocation()
        }
    }
}
