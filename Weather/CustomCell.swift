//
//  TableViewCell.swift
//  Weather
//
//  Created by Sami Chaudry on 24/06/2016.
//  Copyright © 2016 Sami Chaudry. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell
{
    
// MARK: variables
    
    var screenBounds = UIScreen.mainScreen().bounds
    
    var time: UILabel!
    var temperature: UILabel!
    var iconImageView: UIImageView!
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:)")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let cellHeight: CGFloat
        cellHeight = 30
        
        time = UILabel()
        time.frame = CGRectMake(30, 0, screenBounds.width/3, cellHeight)
        time.textColor = UIColor.whiteColor()
        time.font = UIFont.systemFontOfSize(13)
        contentView.addSubview(time)
        
        let iconSize: CGFloat
        iconSize = 20
        
        iconImageView = UIImageView()
        iconImageView = UIImageView(frame:CGRectMake(0, (cellHeight - iconSize)/2, iconSize, iconSize))
        iconImageView.center = CGPointMake(screenBounds.width/2, cellHeight/2)
        contentView.addSubview(iconImageView)
        
        temperature = UILabel()
        temperature.frame = CGRectMake(0, 0, screenBounds.width - 30, cellHeight)
        temperature.textColor = UIColor.whiteColor()
        temperature.textAlignment = .Right
        temperature.font = UIFont.systemFontOfSize(13)
        contentView.addSubview(temperature)
    }
}
