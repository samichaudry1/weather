//
//  ViewController.swift
//  Weather
//
//  Created by Sami Chaudry on 22/06/2016.
//  Copyright © 2016 Sami Chaudry. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import MapKit

import Alamofire
import SwiftyJSON
import ReachabilitySwift


class ViewController: UIViewController, CLLocationManagerDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource
{    
    
// MARK: variables

    let defaults = NSUserDefaults.standardUserDefaults()
    let screenBounds = UIScreen.mainScreen().bounds
    
    var reachability: Reachability?
    var reachable = true
    
    var locationManager: CLLocationManager!
    var usersLocation: CLLocation!
    
    var places: NSMutableArray = []
    var weatherData: NSMutableArray = []
    var placeIndex = 0
    
    var pageControl: UIPageControl!
    var statusLabel: UILabel!
    
    
// MARK: delegate methods
    
    override func viewWillAppear(animated: Bool)
    {
        do
        {
            reachability = try Reachability.reachabilityForInternetConnection()
        }
        catch
        {
            return
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reachabilityChanged(_:)), name: ReachabilityChangedNotification,object: reachability)
        do
        {
            try reachability?.startNotifier()
        }
        catch
        {
            // failed to start reachability notifier
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reloadWeatherViews), name: "reloadWeatherViews", object: nil)
        
        // check if places already exists is user defaults
        var tempArray: NSArray
        if (defaults.objectForKey("places") != nil)
        {
            tempArray = defaults.objectForKey("places") as! NSArray
            places = NSMutableArray(array: tempArray)
        }
        
        self.setupViews()
        self.getUserLocation()
    }
    
    
// MARK: view methods
    
    func setupViews()
    {
        var backgroundImageView: UIImageView
        backgroundImageView = UIImageView(frame:screenBounds)
        backgroundImageView.contentMode = .ScaleAspectFill
        backgroundImageView.image = UIImage(named:"gradient")
        backgroundImageView.tag = 1
        self.view.addSubview(backgroundImageView)
        
        // create status label to display errors e.g. no internet connection
        statusLabel = UILabel()
        statusLabel.frame = CGRectMake(0, 0, screenBounds.width, screenBounds.height)
        statusLabel.textColor = UIColor.whiteColor()
        statusLabel.textAlignment = NSTextAlignment.Center
        statusLabel.font = UIFont.systemFontOfSize(24)
        statusLabel.tag = 2
        self.view.addSubview(statusLabel)
    }
    
    func setupWeatherViews()
    {
        // API response data contains 33 - 40 objects, insert empty objects to to fill data to contain 40 objects
        weatherData = self.insertBlankRows(weatherData)
        
        // create scroll view of places
        let placesScrollView = UIScrollView()
        placesScrollView.frame = screenBounds
        placesScrollView.contentSize = CGSizeMake(screenBounds.width * CGFloat(places.count), screenBounds.height)
        placesScrollView.delegate = self
        placesScrollView.pagingEnabled = true
        placesScrollView.showsHorizontalScrollIndicator = false
        self.view.addSubview(placesScrollView)
    
        // for each place create a weather view
        for place in 0  ..< places.count
        {
            // create json object of weather data for place
            var json = JSON(weatherData[place])
            
            let weatherView = UIView()
            placesScrollView.addSubview(weatherView)
            
            // create label to display place name
            let placeLabel = UILabel()
            placeLabel.textColor = UIColor.whiteColor()
            placeLabel.textAlignment = NSTextAlignment.Center
            placeLabel.text = places[place]["place"] as? String
            placeLabel.font = UIFont.systemFontOfSize(24)
            let placeLabelSize: CGSize = placeLabel.sizeThatFits(CGSizeMake(screenBounds.width, CGFloat.max));
            placeLabel.frame = CGRectMake(0, 0, screenBounds.width, placeLabelSize.height)
            weatherView.addSubview(placeLabel)
            
            // set temperature label font size based on screen size
            var temperatureLabelFontSize: CGFloat
            temperatureLabelFontSize = 40
            
            if screenBounds.height == 568
            {
                temperatureLabelFontSize = 70
            }
            else if screenBounds.height == 667
            {
                temperatureLabelFontSize = 100
            }
            else if screenBounds.height == 736
            {
                temperatureLabelFontSize = 130
            }
            
            // create label to display place temperature
            let temperatureLabel = UILabel()
            temperatureLabel.textColor = UIColor.whiteColor()
            temperatureLabel.textAlignment = NSTextAlignment.Center
            let temperatureText = String(Int(json["list"][0]["main"]["temp_max"].doubleValue - 273.15))
            temperatureLabel.text = String(temperatureText + "°")
            temperatureLabel.font = UIFont.systemFontOfSize(temperatureLabelFontSize, weight: UIFontWeightThin)
            let temperatureLabelSize: CGSize = temperatureLabel.sizeThatFits(CGSizeMake(screenBounds.width, CGFloat.max));
            temperatureLabel.frame = CGRectMake(0, placeLabel.frame.maxY, screenBounds.width, temperatureLabelSize.height)
            weatherView.addSubview(temperatureLabel)
            
            // create label to display weather description
            let weatherDescriptionLabel = UILabel()
            weatherDescriptionLabel.textColor = UIColor.whiteColor()
            weatherDescriptionLabel.textAlignment = NSTextAlignment.Center
            weatherDescriptionLabel.text = String(json["list"][0]["weather"][0]["description"]).capitalizedString
            weatherDescriptionLabel.font = UIFont.systemFontOfSize(15)
            let weatherDescriptionLabelSize: CGSize = weatherDescriptionLabel.sizeThatFits(CGSizeMake(screenBounds.width, CGFloat.max));
            weatherDescriptionLabel.frame = CGRectMake(0, temperatureLabel.frame.maxY, screenBounds.width, weatherDescriptionLabelSize.height)
            weatherView.addSubview(weatherDescriptionLabel)
            
            // create scroll view to display weather data
            let weatherDataScrollView = UIScrollView()
            weatherDataScrollView.frame = CGRectMake(screenBounds.width * CGFloat(place), screenBounds.height - (30 * 11), screenBounds.width, 30 * 9)
            weatherDataScrollView.contentSize = CGSizeMake(screenBounds.width * 5, 200)
            weatherDataScrollView.pagingEnabled = true
            weatherDataScrollView.showsHorizontalScrollIndicator = false
            placesScrollView.addSubview(weatherDataScrollView)
            
            // set the weather view's frame to fit the view in it
            weatherView.frame = CGRectMake(0, 0, screenBounds.width, placeLabel.frame.height + temperatureLabel.frame.height + weatherDescriptionLabel.frame.height)
            
            // center the weather view in the space between the weather data scroll view and the top of the screen
            weatherView.center = CGPointMake(screenBounds.width * CGFloat(place) + screenBounds.width/2, (screenBounds.height - weatherDataScrollView.frame.height)/2 - 20)
            
            // for each day create a day lable and table view
            for dayInt in 0  ..< 5
            {
                let dayNameLabel = UILabel()
                dayNameLabel.frame = CGRectMake(30 + screenBounds.width * CGFloat(dayInt), 0, screenBounds.width - 15, 30)
                dayNameLabel.textColor = UIColor.whiteColor()
                dayNameLabel.font = UIFont.systemFontOfSize(13)
                weatherDataScrollView.addSubview(dayNameLabel)
                
                // add today or tomorrow to day label accordingly
                let dayName = dayNameFromDayInt(dayInt)
                var day = ""
                if dayInt == 0
                {
                    day = "Today"
                }
                else if dayInt == 1
                {
                    day = "Tomorrow"
                }
                let formattedString = NSMutableAttributedString()
                formattedString.bold(dayName).normal("  " + day)
                dayNameLabel.attributedText = formattedString
                
                let tableView = UITableView()
                tableView.frame = CGRectMake(screenBounds.width * CGFloat(dayInt), 30, screenBounds.width, 30 * 8);
                tableView.delegate = self
                tableView.dataSource = self
                tableView.scrollEnabled = false;
                tableView.allowsSelection = false;
                tableView.tag = dayInt + (place * 5);
                tableView.backgroundColor = UIColor.clearColor()
                tableView.separatorColor = UIColor.clearColor()
                weatherDataScrollView.addSubview(tableView)
            }
        }
        
        pageControl = UIPageControl()
        pageControl.frame = CGRectMake(0, 0, screenBounds.width, 30)
        pageControl.center = CGPointMake(screenBounds.width/2, screenBounds.height - 30)
        pageControl.numberOfPages = places.count
        pageControl.currentPage = 0
        self.view.addSubview(pageControl)
        
        let addPlaceButton = UIButton()
        addPlaceButton.frame = CGRectMake(screenBounds.width - 61, screenBounds.height - 61, 60, 60)
        addPlaceButton.setImage(UIImage(named: "addButtonWhite"), forState: UIControlState.Normal)
        addPlaceButton.addTarget(self, action: #selector(addPlacePressed), forControlEvents: .TouchUpInside)
        self.view.addSubview(addPlaceButton)
    }
    
    func reloadWeatherViews()
    {
        if reachable == true
        {
            for view in self.view.subviews
            {
                if view.tag < 1
                {
                    view.removeFromSuperview()
                }
            }
            
            // reset place index counter
            placeIndex = 0
            
            // clear previous weather data
            if weatherData.count > 0
            {
                weatherData.removeAllObjects()
            }
            
            // TODO: update status label
            statusLabel.text = ""
            
            // check if places already exists is user defaults
            var tempArray: NSArray
            if (defaults.objectForKey("places") != nil)
            {
                tempArray = defaults.objectForKey("places") as! NSArray
                places = NSMutableArray(array: tempArray)
            }
            
            self.getForecast(places[placeIndex] as! NSDictionary)
        }
        else
        {
            // TODO: update status label
            statusLabel.text = ""
        }
    }
    
    
// MARK: tableview methods
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return 8
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        // use custom cell subclass
        let cell = CustomCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        let dataIndex = tableView.tag/5
        let row = ((tableView.tag - (dataIndex * 5)) * 8) + indexPath.row
        let json = JSON(weatherData[dataIndex])
    
        // check the row is not blank
        if json["list"][row].null == nil
        {
            cell.time.text = hourFromTimestamp(String(json["list"][row]["dt"]))
            
            let temperature = Int(json["list"][row]["main"]["temp_max"].doubleValue - 273.15)
            cell.temperature.text = String(String(temperature) + "°")
            
            var iconName: String = String(json["list"][row]["weather"][0]["icon"])
            iconName = String(iconName.characters.dropLast())
            cell.iconImageView.image = UIImage(named: iconName)
        }
        
        cell.backgroundColor = UIColor.clearColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 30
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        switch status
        {
        case .NotDetermined:
            // TODO: update status label
            statusLabel.text = ""
        case .AuthorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .AuthorizedAlways:
            statusLabel.text = ""
            locationManager.startUpdatingLocation()
        case .Restricted:
            // TODO: update status label
            self.addDefaultLocation()
            statusLabel.text = ""
        case .Denied:
            // TODO: update status label
            self.addDefaultLocation()
            statusLabel.text = ""
        }
    }
    
    
// MARK: scrollview methods
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
    {
        let pageNumber = round(scrollView.contentOffset.x/scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    
// MARK: location methods
    
    func getUserLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        usersLocation = locations.last! as CLLocation
        locationManager.stopUpdatingLocation()
        
        // reverse gecode user's location to get the place name
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(usersLocation)
        {
            (placemarks, error) -> Void in
            
            let placeArray = placemarks as [CLPlacemark]!
            let placeMark = placeArray?[0] as CLPlacemark!
            
            if let place = placeMark.addressDictionary?["City"] as? String
            {
                let place = ["place": place, "latitude": String(self.usersLocation.coordinate.latitude), "longitude": String(self.usersLocation.coordinate.longitude)]
                
                // update or insert user's location into places
                if self.places.count > 0
                {
                    self.places.replaceObjectAtIndex(0, withObject: place)
                }
                else
                {
                    self.places.insertObject(place, atIndex:0)
                }
                
                // save places to user defaults
                self.defaults.setObject(self.places, forKey: "places")
                
                if self.reachable == true
                {
                    self.getForecast(self.places[self.placeIndex] as! NSDictionary)
                }
                else
                {
                    // TODO: update status label
                    self.statusLabel.text = ""
                }
            }
            
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        // TODO: handle failure
        self.addDefaultLocation()
    }
 
    func addDefaultLocation()
    {
        
        if self.places.count == 0
        {
            // create a default place
            let place = ["place": "Black Swan", "latitude": "53.579461", "longitude": "6.075439"]
            
            // add the place to places
            self.places.insertObject(place, atIndex:0)
            
            // save places to user defaults
            self.defaults.setObject(self.places, forKey: "places")
            
            if self.reachable == true
            {
                self.getForecast(self.places[self.placeIndex] as! NSDictionary)
            }
            else
            {
                // TODO: update status label
                self.statusLabel.text = ""
            }
        }
        else
        {
            // TODO: update status label
            self.statusLabel.text = ""
        }
    }
    
    
// MARK: API methods

    func getForecast(dictionary: NSDictionary)
    {
        let appID = "d883983eaebdd6027aa90a6d85f39589"
        
        // use coordinates from place in places
        let latitude = dictionary["latitude"]!
        let longitude = dictionary["longitude"]!
        
        let parameters = ["APPID": appID,"lat": latitude, "lon": longitude]
        
        let url = "http://api.openweathermap.org/data/2.5/forecast"
            
        Alamofire.request(.GET, url, parameters: parameters)
            .validate().responseJSON { response in
                switch response.result
                {
                case .Success(let data):
                    
                    // add API response data to weather data
                    self.weatherData.addObject(data)
                    
                    // check if there are more places if so get the forecast for the next place
                    if self.places.count > 1
                    {
                        if self.placeIndex < self.places.count - 1
                        {
                            self.placeIndex = self.placeIndex + 1
                            self.getForecast(self.places[self.placeIndex] as! NSDictionary)
                        }
                    }
    
                    // check if we have weather data for each place if so setup the weather views
                    if self.weatherData.count == self.places.count
                    {
                        self.setupWeatherViews()
                    }
                    
                case .Failure(let error):
                    // TODO: handle failure
                    print(error)
                    /*
                    let alert = UIAlertController(title: "Error", message: String(error), preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    */
                }
        }
    }
    
    
// MARK: user actions methods
    
    func addPlacePressed()
    {
        let addPlaceViewController = AddPlaceViewController()
        self.presentViewController(addPlaceViewController, animated: true, completion: nil)
    }
    
    
// MARK: other methods
    
    func reachabilityChanged(note: NSNotification)
    {
        let reachability = note.object as! Reachability
        if reachability.isReachable()
        {
            // TODO: handle reachability change
            reachable = true
            /*
            placeIndex = 0
            
            dispatch_async(dispatch_get_main_queue(),
            {
                self.reloadWeatherViews()
            })
            */
        }
        else
        {
            reachable = false
        }
    }
    
    func insertBlankRows(tempData: NSMutableArray) -> NSMutableArray
    {
        var mutableData: NSMutableDictionary!
        
        for dataInt in 0  ..< tempData.count
        {
            var counter = 0
            
            for row in 0  ..< tempData[dataInt]["list"]!!.count
            {
                let timestamp = tempData[dataInt]["list"]!![row]["dt"] as! NSNumber
                
                let calendar: NSCalendar = NSCalendar.currentCalendar()
                calendar.timeZone = NSTimeZone(abbreviation: "GMT")!
                
                var date = NSDate(timeIntervalSince1970: timestamp.doubleValue)
                let rowDate = calendar.component(.Day, fromDate: date)
                
                date = NSDate()
                let todaysDate = calendar.component(.Day, fromDate: date)
                
                if rowDate == todaysDate
                {
                    counter += 1
                }
            }
            
            let tempArray = tempData[dataInt]["list"] as! NSArray
            let mutableList = NSMutableArray(array: tempArray)
            
            for row in counter  ..< 8
            {
                mutableList.insertObject(NSNull(), atIndex:row)
            }
            
            mutableData = NSMutableDictionary()
            mutableData.setValue(mutableList, forKey:"list")
        
            tempData.replaceObjectAtIndex(dataInt, withObject: mutableData)
        }
        
        return tempData
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    
// MARK: date methods
    
    func hourFromTimestamp(timestamp: NSString) -> String
    {
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(abbreviation: "GMT")!
        
        let date = NSDate(timeIntervalSince1970: timestamp.doubleValue)
        let hour = calendar.component(.Hour, fromDate: date)
        
        var time = String()
        if hour < 10
        {
            time = "0" + String(hour)
        }
        else
        {
            time = String(hour)
        }
        time = time + ":00"
        
        return time
    }
    
    func dayNameFromDayInt(dayInt: Int) -> String
    {
        let day = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: dayInt, toDate: NSDate(), options: NSCalendarOptions.MatchStrictly)
        
        return day!.dayOfTheWeek()!
    }
}


// MARK: extensions

extension NSMutableAttributedString
{
    func bold(text:String) -> NSMutableAttributedString
    {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.appendAttributedString(boldString)
        return self
    }
    
    func normal(text:String) -> NSMutableAttributedString
    {
        let normal =  NSAttributedString(string: text)
        self.appendAttributedString(normal)
        return self
    }
}

extension NSDate
{
    func dayOfTheWeek() -> String?
    {
        let weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Satudrday"]
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        let components: NSDateComponents = calendar.components(.Weekday, fromDate: self)
        
        return weekdays[components.weekday - 1]
    }
}

