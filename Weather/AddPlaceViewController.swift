//
//  AddPlaceViewController.swift
//  Weather
//
//  Created by Sami Chaudry on 26/06/2016.
//  Copyright © 2016 Sami Chaudry. All rights reserved.
//

import UIKit
import GoogleMaps

class AddPlaceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{    
    
// MARK: variables
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var screenBounds = UIScreen.mainScreen().bounds
    
    var places: NSMutableArray = []
    
    let tableView = UITableView()
    
    
// MARK: delegate methods

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // check if places already exists is user defaults
        var tempArray: NSArray
        if (defaults.objectForKey("places") != nil)
        {
            tempArray = defaults.objectForKey("places") as! NSArray
            places = NSMutableArray(array: tempArray)
        }
        
        self.setupViews()
    }

    
// MARK: view methods
    
    func setupViews()
    {
        let navigationBar = UINavigationBar()
        navigationBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 64)
        navigationBar.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(navigationBar)
        
        let navigationItem = UINavigationItem()
        navigationItem.title = "Places"
        let rightButton =  UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(doneButtonPressed))
        navigationItem.rightBarButtonItem = rightButton
        navigationBar.items = [navigationItem]
        
        tableView.frame = CGRectMake(0, 64, screenBounds.width, screenBounds.height - 64);
        tableView.delegate = self
        tableView.dataSource = self
        self.view.insertSubview(tableView, belowSubview: navigationBar)
    }
    
    
// MARK: tableview methods
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return places.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        // add an extra row to the table to allow the user to add a place
        if indexPath.row == places.count
        {
            let imageView = UIImageView()
            imageView.frame = CGRectMake(15, 12, 20, 20)
            imageView.image = UIImage(named: "addButtonBlack")
            cell.contentView.addSubview(imageView)
            
            let label = UILabel()
            label.frame = CGRectMake(42, 0, screenBounds.width - 42, 44)
            label.font = cell.textLabel?.font
            label.text = "Add a place"
            cell.contentView.addSubview(label)
        }
        else
        {
            cell.textLabel?.text = places[indexPath.row]["place"] as? String
            cell.selectionStyle = .None
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.row == places.count
        {
            self.addPlacePressed()
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        // only allow the user to delete places that are not the first (user location or default place) or last row (add a place)
        if indexPath.row == places.count
        {
            return false
        }
        else if indexPath.row == 0
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if (editingStyle == UITableViewCellEditingStyle.Delete)
        {
            // remove the place from places
            places.removeObjectAtIndex(indexPath.row)
            
            // save places to user defaults
            defaults.setObject(places, forKey: "places")
            
            tableView.reloadData()
            
            // reload weather views as places has changed
            NSNotificationCenter.defaultCenter().postNotificationName("reloadWeatherViews", object: nil)
        }
    }
    

// MARK: user actions methods
    
    func addPlacePressed()
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.presentViewController(autocompleteController, animated: false, completion: nil)
    }
    
    func doneButtonPressed()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
// MARK: other methods
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}


// MARK: extensions

extension AddPlaceViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        
        let place = ["place": place.name, "latitude": place.coordinate.latitude, "longitude": place.coordinate.longitude]
        
        // add the place to places
        places.insertObject(place, atIndex:places.count)
        
        // save places to user defaults
        defaults.setObject(places, forKey: "places")
        
        tableView.reloadData()
        
        // reload weather views as places has changed
        NSNotificationCenter.defaultCenter().postNotificationName("reloadWeatherViews", object: nil)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // TODO: handle the error.
        print("Error: ", error.description)
    }
    
    // User canceled the operation.
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
}
